#
# 🜏 - Makefile
#
VENDOR  ?= lambdadeveloper
PROJECT ?= isat
TAG     ?= latest
PORT    ?= 80


DEFAULT: help

: ##

start: launch-database ## Starts project locally…
	dotnet run --project src/ISAT.Developer.Exam.Web/

build: ## Builds Docker container…
	docker build --file Dockerfile . --tag $(VENDOR)/$(PROJECT):$(TAG)

dotnet: ## Build locally using .Net SDK
	dotnet build

launch: launch-docker ## Starts Docker container…
launch-docker: \
	launch-database \
	launch-runtime

launch-runtime:
	docker run --rm --publish 80:80 --publish 443:443 $(VENDOR)/$(PROJECT):$(TAG)

launch-database:
	docker-compose --file ./solution-utilities/docker/docker-compose.yml \
		up --detach --remove-orphans

stop: ## Shutdown database container…
	docker-compose --file ./solution-utilities/docker/docker-compose.yml \
		down --remove-orphans

: ##

install: clear migrate ## Installs dependencies in a clear environment…

clear: ## Drop database…
	dotnet ef database --project src/ISAT.Developer.Exam.Infrastructure/ drop

migrate: ## Apply migrations…
	dotnet ef database --project src/ISAT.Developer.Exam.Infrastructure/ update

: ##

.PHONY: tests
tests: ## Executes test suite…
	dotnet test tests/ --verbosity minimal --nologo

: ##

help: ## Shows this help.
	@grep -E '''#''#''' $(MAKEFILE_LIST) | awk 'BEGIN { FS=":.*?#" } \
	/(\w+:.*|\s+)#/ { printf("\033[1m%-10s\033[0m%s\n", $$1, $$2); }'

: ##