#
# 🜏 - ISAT.Developer.Exam.Web
#
FROM mcr.microsoft.com/dotnet/sdk:3.1-alpine AS build
WORKDIR /source

COPY . .
RUN dotnet publish src/ISAT.Developer.Exam.Web/ISAT.Developer.Exam.Web.csproj \
        -c release -o /app -r linux-musl-x64 \
        --self-contained true \
            /p:PublishTrimmed=true \
            /p:PublishReadyToRun=true \
            /p:PublishSingleFile=true

FROM mcr.microsoft.com/dotnet/runtime-deps:3.1-alpine AS runtime
WORKDIR /app

COPY --from=build /app .

ENTRYPOINT [ "./ISAT.Developer.Exam.Web" ]