using System;
using Xunit;

using ISAT.Developer.Exam.Core.Entities;

namespace tests
{
    public class ResourceTest
    {
        [Fact]
        public void IsThisWorking()
        {
            Assert.True(true, "The test suite was correctly configured.");
        }

        [Fact]
        public void IsCreatingPersons()
        {
            Person person = Person.CreateNewPerson("John", "Murowaniecki", "jmurowaniecki@gmail.com", 36);

            Assert.True(person.IsValid(), "We're creating persons properly.");
        }

        [Fact]
        public void IsBlockingUnderage()
        {
            Person person = Person.CreateNewPerson("John", "Murowaniecki", "jmurowaniecki@gmail.com", 6);

            Assert.True(!person.IsValid(), "We're filtering underages.");
        }

        [Fact]
        public void IsBlockingElderly()
        {
            Person person = Person.CreateNewPerson("John", "Murowaniecki", "jmurowaniecki@gmail.com", 136);

            Assert.True(!person.IsValid(), "We're filtering elders.");
        }

        [Fact]
        public void IsBlockingShortNames()
        {
            Person person = Person.CreateNewPerson("Jon", "Murowaniecki", "jmurowaniecki@gmail.com", 36);

            Assert.True(!person.IsValid(), "We're filtering short names.");
        }

        [Fact]
        public void IsBlockingShortLastNames()
        {
            Person person = Person.CreateNewPerson("John", "Jow", "jmurowaniecki@gmail.com", 36);

            Assert.True(!person.IsValid(), "We're filtering short last names.");
        }

        [Fact]
        public void IsBlockingShortEmails()
        {
            Person person = Person.CreateNewPerson("John", "Murowaniecki", "j@il.m", 36);

            Assert.True(!person.IsValid(), "We're filtering short e-mails.");
        }

        [Fact]
        public void IsBlockingLongNames()
        {
            Person person = Person.CreateNewPerson("John************************************************************************************************************************************************************************************************************", "Murowaniecki", "j@il.m", 36);

            Assert.True(!person.IsValid(), "We're filtering long names.");
        }

        [Fact]
        public void IsBlockingLongLastNames()
        {
            Person person = Person.CreateNewPerson("John", "Murowaniecki************************************************************************************************************************************************************************************************************", "j@il.m", 36);

            Assert.True(!person.IsValid(), "We're filtering long last names.");
        }
    }
}
