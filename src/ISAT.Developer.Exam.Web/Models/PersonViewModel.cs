using System.ComponentModel;

namespace ISAT.Developer.Exam.Web.Models
{
    public class PersonViewModel
    {
        public long Id { get; set; }

        [DisplayName("First name")]
        public string FirstName { get; set; }

        [DisplayName("Last name")]
        public string LastName { get; set; }

        [DisplayName("E-mail")]
        public string Email { get; set; }

        [DisplayName("Age")]
        public int Age { get; set; }

        public string getClass = "col-md-10 form-control";
    }
}