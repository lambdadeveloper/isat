using System;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using ISAT.Developer.Exam.Web.Models;
using ISAT.Developer.Exam.Core.Entities;
using ISAT.Developer.Exam.Core.Services;
using ISAT.Developer.Exam.Core.Interfaces;

namespace ISAT.Developer.Exam.Web.Controllers
{
    public class PersonsController : BaseController
    {
        private readonly ILogger<PersonsController> _logger;

        private readonly IPersonService _personService;

        public PersonsController(ILogger<PersonsController> logger, IPersonService personService)
        {
            _logger = logger;
            _personService = personService;
        }

        public IActionResult Index()
        {
            var persons = _personService.GetAll();

            return View(persons.Select(person => new PersonViewModel()
            {
                Id        = person.Id,
                FirstName = person.FirstName,
                LastName  = person.LastName,
                Email     = person.Email,
                Age       = person.Age
            }));
        }

        [HttpGet]
        public IActionResult Create()
        {

            var personViewModel = new PersonViewModel();
            return View(personViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind] PersonViewModel personViewModel)
        {
            if (!ModelState.IsValid) {
                return View(personViewModel);
            }

            var personEntity = Person.CreateNewPerson(personViewModel.FirstName, personViewModel.LastName, personViewModel.Email, personViewModel.Age);

            if (ResponseHasErrors(_personService.Insert(personEntity))) {
                return View(personViewModel);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(long? id)
        {
            if (id == null) {
                return NotFound();
            }

            var personEntity = _personService.GetById(id.Value);
            if (personEntity == null) {
                return NotFound();
            }

            var personViewModel = new PersonViewModel()
            {
                Id        = personEntity.Id,
                FirstName = personEntity.FirstName,
                LastName  = personEntity.LastName,
                Email     = personEntity.Email,
                Age       = personEntity.Age,
            };

            return View(personViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit([Bind] PersonViewModel personViewModel)
        {
            if (!ModelState.IsValid) {
                return View(personViewModel);
            }

            var personEntity = Person.ToPerson(personViewModel.Id, personViewModel.FirstName, personViewModel.LastName, personViewModel.Email, personViewModel.Age);

            if (ResponseHasErrors(_personService.Update(personEntity))) {
                return View(personViewModel);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(long? id)
        {
            if (id == null) {
                return NotFound();
            }

            var personEntity = _personService.GetById(id.Value);
            if (personEntity == null) {
                return NotFound();
            }

            var personViewModel = new PersonViewModel()
            {
                Id        = personEntity.Id,
                FirstName = personEntity.FirstName,
                LastName  = personEntity.LastName,
                Email     = personEntity.Email,
                Age       = personEntity.Age,
            };

            return View(personViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete([Bind] PersonViewModel personViewModel)
        {
            if (!ModelState.IsValid || ResponseHasErrors(_personService.Delete(personViewModel.Id))) {
                return View(personViewModel);
            }

            return RedirectToAction("Index");
        }
    }
}
