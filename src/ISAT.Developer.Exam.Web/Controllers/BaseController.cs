using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace ISAT.Developer.Exam.Web.Controllers
{
    public class BaseController : Controller
    {
        private readonly ICollection<string> _errors = new List<string>();

        protected bool ResponseHasErrors(ValidationResult validate)
        {
            if (validate == null || validate.IsValid) {
                return false;
            }

            foreach (var error in validate.Errors) {
                ModelState.AddModelError(string.Empty, error.ErrorMessage);
            }

            return true;
        }
    }
}
