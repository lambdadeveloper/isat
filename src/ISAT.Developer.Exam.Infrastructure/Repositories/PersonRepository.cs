using ISAT.Developer.Exam.Core.Entities;
using ISAT.Developer.Exam.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ISAT.Developer.Exam.Infrastructure.ORM.Repositories
{
    public class PersonRepository : Repository<Person>, IPersonRepository
    { }
}
