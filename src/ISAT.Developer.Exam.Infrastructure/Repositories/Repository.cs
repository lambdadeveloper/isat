﻿using System;
using System.Linq;
using System.Collections.Generic;

using Microsoft.EntityFrameworkCore;

using ISAT.Developer.Exam.Core.Entities;
using ISAT.Developer.Exam.Core.Interfaces;
using ISAT.Developer.Exam.Infrastructure.ORM.Contexts;

namespace ISAT.Developer.Exam.Infrastructure.ORM.Repositories
{
    public class Repository<TEntity> : IDisposable, IRepository<TEntity> where TEntity : BaseEntity<TEntity>
    {
        #region properties

        protected EFContextDB Db;

        protected DbSet<TEntity> DbSet;

        #endregion

        #region ctor

        public Repository()
        {
            Db = new EFContextDB();

            DbSet = Db.Set<TEntity>();
        }

        #endregion

        #region methods

        public virtual TEntity FirstOrDefault(Func<TEntity, bool> func)
        {
            return DbSet.AsNoTracking().FirstOrDefault(func);
        }

        public virtual void Insert(TEntity entity)
        {
            entity.SetLastAction("");
            DbSet.Add(entity);
            Db.SaveChanges();
        }

        public virtual void Delete(long id)
        {
            TEntity entity = DbSet.FirstOrDefault(person => person.Id == id);

            if (entity != null) {
                DbSet.Remove(entity);
                Db.SaveChanges();
            }
        }

        public virtual void Update(TEntity entity)
        {
            // Db.Entry(entity).State = EntityState.Detached;
            Db.Entry(entity).State = EntityState.Modified;
            // Db.Entry(entity).Navigation("Id").IsModified = false;


            entity.SetLastAction("");

            DbSet.Update(entity);
            Db.SaveChanges();
        }

        public virtual TEntity GetById(long id)
        {
            return DbSet.AsNoTracking().FirstOrDefault(person => person.Id == id);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return DbSet.AsNoTracking().ToList();
        }

        public void Dispose()
        {
            Db.Dispose();
        }

        #endregion
    }
}