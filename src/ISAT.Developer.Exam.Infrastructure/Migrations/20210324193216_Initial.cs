﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ISAT.Developer.Exam.Infrastructure.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Persons",
                columns: schema => new
                    {
                        Id        = schema.Column<long>(nullable: false).Annotation("SqlServer:Identity", "1, 1"),
                        FirstName = schema.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false),
                        LastName  = schema.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false),
                        Email     = schema.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false),
                        Age       = schema.Column<int>(nullable: false),

                        // Undocumented but required fields by BaseEntity…
                        LastAction      = schema.Column<string>(nullable: true),
                        LastLoginAction = schema.Column<string>(nullable: true),
                        LastUpdatedDate = schema.Column<DateTime>(nullable: true),
                        CreatedDate     = schema.Column<DateTime>(nullable: false),
                        CascadeMode     = schema.Column<int>(nullable: false)
                    },
                    constraints: schema =>
                    {
                        schema.PrimaryKey("PK_Persons", person => person.Id);
                    }
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(name: "Persons");
        }
    }
}
