using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using ISAT.Developer.Exam.Core.Entities;

namespace ISAT.Developer.Exam.Infrastructure.ORM.Contexts
{
    public class PersonConfig : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> entity)
        {
            entity.ToTable("Persons");
            entity.HasKey  (schema => schema.Id);
            entity.Property(schema => schema.FirstName).IsRequired().HasMaxLength(255).HasColumnType("varchar(255)");
            entity.Property(schema => schema.LastName ).IsRequired().HasMaxLength(255).HasColumnType("varchar(255)");
            entity.Property(schema => schema.Email    ).IsRequired().HasMaxLength(255).HasColumnType("varchar(255)");
            entity.Ignore  (schema => schema.ValidationResult);
        }
    }
}
