using System;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using ISAT.Developer.Exam.Infrastructure.Utilities;

namespace ISAT.Developer.Exam.Infrastructure.ORM.Contexts
{
    public class EFContextDB : DbContext
    {
        #region properties

        #endregion

        #region ctor

        public EFContextDB(DbContextOptions<EFContextDB> options) : base(options)
        { }

        public EFContextDB()
        { }

        #endregion

        #region dbsets

        #endregion

        #region methods
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new PersonConfig());
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlServer(GetConnectionStringUtility
                    .GetConnectionString("SQLConnection"));

            optionsBuilder.EnableSensitiveDataLogging();
        }
        #endregion
    }
}
