﻿using System;

using Microsoft.Extensions.DependencyInjection;

using ISAT.Developer.Exam.Core.Interfaces;
using ISAT.Developer.Exam.Core.Services;
using ISAT.Developer.Exam.Infrastructure.ORM.Contexts;
using ISAT.Developer.Exam.Infrastructure.ORM.Repositories;

namespace ISAT.Developer.Exam.Infrastructure.IoC
{
    public class IoCServiceRegister
    {
        public static void Register(IServiceCollection services)
        {
            //Core
            services.AddTransient<EFContextDB, EFContextDB>();

            //ORM
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IPersonRepository, PersonRepository>();
            services.AddScoped<IPersonService,    PersonService>();
        }
    }

    internal class Lazier<T> : Lazy<T> where T : class
    {
        public Lazier(IServiceProvider provider) : base(() => provider.GetRequiredService<T>())
        { }
    }
}
