using System;
using FluentValidation;
using FluentValidation.Results;

namespace ISAT.Developer.Exam.Core.Entities
{
    public class Person : BaseEntity<Person>
    {
        public string FirstName { get; private set; }
        public string LastName  { get; private set; }
        public string Email     { get; private set; }
        public int    Age       { get; private set; }

        public static Person CreateNewPerson(string firstName, string lastName, string email, int age)
        {
            var person = new Person() {
                FirstName   = firstName,
                LastName    = lastName,
                Email       = email,
                Age         = age,
                CreatedDate = DateTime.Now
            };

            person.ValidateFirstName();
            person.ValidateLastName();
            person.ValidateEmail();
            person.ValidateAge();

            return person;
        }

        public static Person ToPerson(long id, string firstName, string lastName, string email, int age)
        {
            var person = new Person() {
                Id        = id,
                FirstName = firstName,
                LastName  = lastName,
                Email     = email,
                Age       = age
            };

            person.ValidateId();
            person.ValidateFirstName();
            person.ValidateLastName();
            person.ValidateEmail();
            person.ValidateAge();

            return person;
        }

        public override bool IsValid()
        {
            ValidationResult = Validate(this);
            return ValidationResult.IsValid;
        }

        private void ValidateId()
        {
            RuleFor(c => c.Id)
                .GreaterThan(0)
                    .WithMessage("Invalid ID");
        }

        private void ValidateFirstName(int minLen = 4, int maxLen = 255)
        {
            RuleFor(c => c.FirstName)
                .NotEmpty()
                    .WithMessage("First name required.")
                .MinimumLength(minLen)
                    .WithMessage($"The minimum length for first name is {minLen} chars.")
                .MaximumLength(maxLen)
                    .WithMessage($"The maximum length for first name is {maxLen} chars.");
        }

        private void ValidateLastName(int minLen = 4, int maxLen = 255)
        {
            RuleFor(c => c.LastName)
                .NotEmpty()
                    .WithMessage("Last name is required.")
                .MinimumLength(minLen)
                    .WithMessage($"The minimum length for last name is {minLen} chars.")
                .MaximumLength(maxLen)
                    .WithMessage($"The maximum length for last name is {maxLen} chars.");
        }

        private void ValidateEmail(int minLen = 7, int maxLen = 255)
        {
            RuleFor(c => c.Email)
                .NotEmpty()
                    .WithMessage("E-mail required.")
                .MinimumLength(minLen)
                    .WithMessage($"The minimum length for e-mail is {maxLen} chars.")
                .MaximumLength(maxLen)
                    .WithMessage($"The maximum length for e-mail is {maxLen} chars.");
        }

        private void ValidateAge(int minAge = 10, int maxAge = 100)
        {
            RuleFor(c => c.Age)
                .GreaterThanOrEqualTo(minAge)
                    .WithMessage($"Age cannot be less than {minAge} years.")
                .LessThanOrEqualTo(maxAge)
                    .WithMessage($"Age cannot be greater than {maxAge} years.");
        }
    }
}
