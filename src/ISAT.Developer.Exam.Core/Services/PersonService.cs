using ISAT.Developer.Exam.Core.Entities;
using ISAT.Developer.Exam.Core.Interfaces;
using FluentValidation;
using FluentValidation.Results;
using System.Collections.Generic;
using System.Linq;
using System;

namespace ISAT.Developer.Exam.Core.Services
{
    public class PersonService : Service<Person>, IPersonService
    {
        public PersonService(IPersonRepository PersonRepository) : base(PersonRepository)
        { }

        public Person GetById(long ID)
        {
            return _repository.GetById(ID);
        }

        public IEnumerable<Person> GetAll()
        {
            return _repository.GetAll();
        }

        public override ValidationResult Insert(Person person)
        {
            return validationOfPerson(person, _repository.Insert);
        }

        public override ValidationResult Update(Person person)
        {
            return validationOfPerson(person, _repository.Update);
        }

        public ValidationResult validationOfPerson(Person person, Action<Person> callbackAction)
        {
            if (!person.IsValid()) {
                return person.ValidationResult;
            }

            if (_repository.FirstOrDefault(entity => entity.Id != person.Id && entity.Email == person.Email) != null) {
                IEnumerable<ValidationFailure> failures = new List<ValidationFailure>() {
                    new ValidationFailure("Email", $"E-mail {person.Email} already registered.")
                };

                return new ValidationResult(failures);
            }

            if (_repository.FirstOrDefault(entity => entity.Id != person.Id && entity.FirstName.ToLower() == person.FirstName.ToLower() && entity.LastName.ToLower() == person.LastName.ToLower()) != null) {
                IEnumerable<ValidationFailure> failures = new List<ValidationFailure>() {
                    new ValidationFailure("Name", $"First name {person.FirstName} and {person.LastName} already registered.")
                };

                return new ValidationResult(failures);
            }

            callbackAction(person);

            return null;
        }
    }
}
