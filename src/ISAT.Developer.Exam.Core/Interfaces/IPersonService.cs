using ISAT.Developer.Exam.Core.Entities;
using System.Collections.Generic;

namespace ISAT.Developer.Exam.Core.Interfaces
{
    public interface IPersonService : IService<Person>
    {
        Person GetById(long id);

        IEnumerable<Person> GetAll();
    }
}
