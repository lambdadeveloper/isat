using ISAT.Developer.Exam.Core.Entities;

namespace ISAT.Developer.Exam.Core.Interfaces
{
    public interface IPersonRepository : IRepository<Person>
    { }
}
